#ifndef SERVICECONTROLLER_H
#define SERVICECONTROLLER_H

#define FILE_DEBUG

#include <QString>
#include <QProcess>
#include <QStringList>
#include <QTextStream>
#include <QRegularExpression>
#include <QByteArray>
#ifdef TEMP_DEBUG
#include <QDebug>
#elif defined(FILE_DEBUG)
#include "SimpleFileLoger/log.h"
#define logfilename "/tmp/LinTorServiceController.log"
#endif

class ServiceController{
public  :
    QString RunCommand  (const QString &cmd, const QStringList &arg);
    bool    Status      (const QString &process);
    bool    CheckRoot   (void);
};

#endif // SERVICECONTROLLER_H
