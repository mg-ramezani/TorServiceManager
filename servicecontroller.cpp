#include "servicecontroller.h"

QString ServiceController::RunCommand(const QString &cmd, const QStringList &arg){
    QProcess* proc = new QProcess;
    if (arg.isEmpty())
        proc->start (cmd);
    else
        proc->start (cmd,arg);
#ifdef FILE_DEBUG
    log::breakinglog("Process " + cmd + " Started [ServiceControl::RunCommand]",logfilename);
#elif defined(TEMP_DEBUG)
    qDebug() << "Process " + cmd + " Started [ServiceControl::RunCommand]"
#endif
    QByteArray data;
    proc->write(data);
    proc->waitForBytesWritten();
    proc->closeWriteChannel();
    proc->waitForFinished();
    QTextStream output ( proc->readAllStandardOutput() );
    delete proc;
    return output.readAll().trimmed();
}

bool ServiceController::Status(const QString &process){
    QRegularExpression myRegex(" active ");
    QRegularExpressionMatch match ( myRegex.match(RunCommand(
        "systemctl",
        QStringList() << "status" << process
        )) );
    return match.hasMatch();
}

bool ServiceController::CheckRoot(){

}
