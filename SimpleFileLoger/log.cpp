#include "log.h"

void log::breakinglog(const QString &data, const QString &FileName){
    QFile myFile (FileName);
    if (myFile.open(QFile::WriteOnly | QFile::Text)){
            QTextStream stdout (&myFile);
            stdout << QDate::currentDate().toString()
                   << "-"
                   << QTime::currentTime().toString()
                   << "--"
                   << data;
        }
    myFile.flush();
    myFile.close();
}
