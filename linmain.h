#ifndef LINMAIN_H
#define LINMAIN_H

#include <QDialog>
#include "servicecontroller.h"

namespace Ui {
    class LinMain;
}

class LinMain : public QDialog
{
    Q_OBJECT

public:
    explicit LinMain(QWidget *parent = nullptr);
    ~LinMain();

private slots:
    void on_pushButton_clicked();

private:
    Ui::LinMain *ui;
    ServiceController *service;

};

#endif // LINMAIN_H
