#include "linmain.h"
#include "ui_linmain.h"
#include "servicecontroller.h"

LinMain::LinMain(QWidget *parent) :
                                    QDialog(parent),
                                    ui(new Ui::LinMain){

    ui->setupUi(this);

    if (service->Status("tor") == true){
            ui->pushButton->setStyleSheet("background-color :  green;");
        }else {
            ui->pushButton->setStyleSheet("background-color : red;");
        }

}

LinMain::~LinMain(){
    delete ui;
}

void LinMain::on_pushButton_clicked(){
    if (service->Status("tor") == true){
            ui->pushButton->setStyleSheet("background-color : red;"
                                          "font: 9pt \"Monaco\";"
                                          "padding: 20px;"
                                          "border-radius:  70px ;"
                                                                    );
            service->RunCommand("xfce4-terminal",QStringList() << "--command=systemctl stop tor");
        } else if (service->Status("tor") == false){
            ui->pushButton->setStyleSheet("background-color : green;"
                                          "font: 9pt \"Monaco\";"
                                          "padding: 20px;"
                                          "border-radius:  70px ;"
                                                                    );
            service->RunCommand("xfce4-terminal",QStringList() << "--command=systemctl start tor");
        }
}
